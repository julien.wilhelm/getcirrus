---
title: 1. Comprendre les rôles
category: "2. Partager cirrus"
layout: default
---

Dans cirrus, la gestion des droits se fait par l'intermédiaire de rôles. Ainsi, en fonction du rôle qui est le leur, les utilisateurs peuvent réaliser plus ou moins d'actions. 

Sur cette page :

* [Visualiseur](#visualiseur)
* [Éditeur](#éditeur)
* [Propriétaire](#propriétaire)

## Visualiseur ##

Les utilisateurs de type visualiseur sont les ceux qui ont le moins de privilèges. Ils peuvent :

* Accéder en lecture seule à tout contenu public (prévisualiser, télécharger).
* Être autorisés à accéder, en lecture seule toujours, à des [dossiers protégés](./creer-proteger-supprimer-un-dossier#protéger-un-dossier-et-son-contenu).
* Utiliser la fonctionnalité de recherche.

Les utilisateurs de type visualiseur ne sont pas limités en nombre.

## Éditeur ##

Les utilisateurs de type éditeur disposent de droits identiques à ceux des [visualiseurs](#visualiseur), mais peuvent en plus :

* Accéder en écriture à tout contenu public (mise en ligne, déplacement, renommage, etc.).
* Accéder en écriture aux [dossiers protégés](./creer-proteger-supprimer-un-dossier#protéger-un-dossier-et-son-contenu) auxquels ils sont invités.
* [Définir des règles d'accès spécifiques aux dossiers](./creer-proteger-supprimer-un-dossier#protéger-un-dossier-et-son-contenu).

Les utilisateurs de type éditeur ne sont pas limités en nombre.

## Propriétaire ##

L'utilisateur de type propriétaire dispose de droits identiques à ceux des [éditeurs](#éditeur), mais peut en plus :

* Accéder, sans aucune restriction, au contenu entier d'un cirrus.
* [Gérer la corbeille](./gerer-la-corbeille#doc).
* Utiliser les fonctionnalités de l'interface d'administration.

Il ne peut y avoir qu'un seul utilisateur de type propriétaire par cirrus.