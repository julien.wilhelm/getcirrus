---
title: 4. Mettre en ligne, supprimer des fichiers
category: "1. Utiliser cirrus"
layout: default
---

Sur cette page :

* [Mettre en ligne un fichier](#mettre-en-ligne-un-fichier)
* [Supprimer un fichier](#supprimer-un-fichier)

## Mettre en ligne un fichier ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

1. Cliquez sur le bouton <svg viewBox="3 3 18 18"><title>Envoyer des fichiers</title><path d="M10.4971 12.9823L10 12.4853L12.4853 10L14.9706 12.4853L14.4735 12.9823L12.8368 11.3456V16H12.1338V11.3456L10.4971 12.9823Z"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M15.1571 3H6.63636C5.73262 3 5 3.73262 5 4.63636V19.3636C5 20.2674 5.73262 21 6.63636 21H18.0909C18.9946 21 19.7273 20.2674 19.7273 19.3636V7.57019L15.1571 3ZM6.63636 4.63636H13.1818V7.90909C13.1818 8.81283 13.9144 9.54545 14.8182 9.54545H18.0909V19.3636H6.63636V4.63636ZM14.8182 7.90909V4.97527L17.752 7.90909H14.8182Z"></path></svg> situé dans le coin supérieur de cirrus.
2. Sélectionnez le fichier à télécharger grâce à l'explorateur de fichiers propre à votre système d'exploitation. Sélectionnez-en plusieurs pour un envoi groupé.
    * Les caractères spéciaux non supportés seront automatiquement remplacés par des tirets.
    * Si cirrus contient déjà un fichier du même nom que celui envoyé à cet emplacement, le nouveau sera renommé pour empêcher toute perte inattendue de contenu. 
    * Certaines limitations peuvent s'appliquer (voir la section prérequis en page "[Installer cirrus](./installer-cirrus#doc)").
3. Validez l'opération avec le bouton `Ouvrir`. L'espace de notification, qui apparait en bas de l'écran, permet de suivre la progression de la mise en ligne.

## Supprimer un fichier ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

Pour supprimer un fichier de cirrus, reportez-vous à la page "[Supprimer un élément](./supprimer-un-element#doc)".