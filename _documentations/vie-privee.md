---
title: Vie privée
category: "4. À propos"
layout: default
---

Pour une fois, oublions l'adage : "Si c'est gratuit, c'est que c'est vous le produit". Avec cirrus, une seule philosophie à ce sujet : "**vos données, votre vie privée**". Ainsi, vos données personnelles ne sont collectées **à aucun moment** de votre relation avec cirrus :

* Ni pendant son installation.
* Ni pendant votre utilisation.
* Ni même maintenant, sur cette documentation.

Vous appréciez cirrus ?  
[Faites-le savoir](https://awebsome.fr/#contact). 