---
title: Garanties
category: "4. À propos"
layout: default
---

Bien que cirrus soit en téléchargement libre et est prêt à être déployé, il est impossible pour son créateur de vérifier la conformité des installations de son application ; de fait, **il appartient aux utilisateurs de cirrus de s'assurer du bon fonctionnement de ce dernier** et d'assumer pleinement les conséquences, positives comme négatives, liées à son exploitation.

Si vous pensez être ou avoir été victime d'un bogue avec cirrus, vous pouvez [ouvrir une *issue* dédiée](https://gitlab.com/julien.wilhelm/cirrus/-/issues) si le problème n'est pas déjà listé ou, dans le cas contraire, compléter les informations disponibles des vôtres en vue de faciliter la résolution du dysfonctionnement visé.