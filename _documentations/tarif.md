---
title: Tarif (gratuit)
category: "4. À propos"
layout: default
---

Cette page ne vous apprendra rien, sinon que oui, **cirrus est parfaitement gratuit**. Et que vous pouvez **[en soutenir financièrement le développement](https://awebsome.fr/#contact)**, sans que rien ne vous y oblige. 