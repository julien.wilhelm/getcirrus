---
title: 1. Installer cirrus
category: "1. Utiliser cirrus"
layout: default
---

Sur cette page :

1. [Prérequis](#prérequis)
2. [Téléchargement](#téléchargement)
3. [Installation](#installation)

## Prérequis ##

Véritable modèle de frugalité logicielle, cirrus a besoin de peu pour être opérationnel. 

Voici la liste des principaux prérequis à son installation :

* Un serveur web Apache prenant en charge PHP version 7.4 minimum.
* Un espace de stockage de 200 Ko d'espace libre pour la couche applicative. La capacité réelle requise dépend des données que vous mettez en ligne.
* Un navigateur web moderne avec JavaScript activé (au hasard : [Firefox](https://www.mozilla.org/fr/firefox/new/)).

Conçu sans dépendances fortes ni base de données, cirrus peut être déployé à moindre coût, y compris sur un serveur mutualisé d'entrée de gamme. Le cas échéant, assurez-vous que l'hébergeur permette l'édition des constantes PHP suivantes, dont la valeur par défaut est parfois faible :

* `post_max_size` : taille maximale autorisée pour le téléversement multiple de fichiers.
* `upload_max_filesize` : poids maximal autorisé par fichier téléversé).

## Téléchargement ##

Rendez-vous sur [la page de téléchargement officielle de cirrus](https://gitlab.com/julien.wilhelm/cirrus/-/tags) sur GitLab et sélectionnez la dernière version (numéro le plus élevé, en tête de liste) pour profiter des dernières fonctionnalités, mais aussi de tous les correctifs de performance et de sécurité éventuels.

## Installation ##

1. Décompressez l'archive obtenue dans la section ci-dessus.
2. Sélectionnez les éléments suivants (les sources étant incluses, mais non requises) :
    * Dossier "app"
    * Dossier "pages"
    * Fichier "index.php"
    * Fichier ".htaccess" (attention, ce fichier est caché)
2. Copiez-les sur un serveur web (racine ou sous-dossier spécifique).
3. Depuis votre navigateur web, rendez-vous à l'URL correspondante telle que "https://votre-serveur/chemin/vers/cirrus/" pour être redirigé vers l'assistant de création du compte propriétaire.
4. Une fois le compte propriétaire validé, cirrus est prêt à être utilisé.