---
title: 2. Mettre à jour cirrus
category: "1. Utiliser cirrus"
layout: default
---
 
Sur cette page :

* [Pourquoi mettre à jour cirrus ?](#pourquoi-mettre-à-jour-cirrus)
* [Où obtenir les mises à jour ?](#où-obtenir-les-mises-à-jour)
* [Préparer le passage d'une version 1.5.2 (ou inférieure) à une version 1.6.0 (ou supérieure)](#préparer-le-passage-dune-version-152-ou-inférieure-à-une-version-160-ou-supérieure)
* [Mettre à jour cirrus](#mettre-à-jour-cirrus)

## Pourquoi mettre à jour cirrus ? ##

Dans le monde du logiciel, les mises à jour apportent généralement leur lot de nouveautés et de correctifs. En contrepartie, il est fréquent de voir les performances de l'application se dégrader. Cirrus est pensé différemment : son but n'est pas d'en proposer toujours plus, mais de se solidifier sans perdre de vue les objectifs de sobriété numérique qu'il défend.

## Où obtenir les mises à jour ? ##

Les mises à jour de cirrus sont disponibles sur [la page de téléchargement officielle de cirrus](https://gitlab.com/julien.wilhelm/cirrus/-/tags).

Pour savoir si votre cirrus doit être mis à jour, repérez le numéro de version actuelle situé dans le coin inférieur droit de l'application (1.X.X) puis comparez-le aux versions proposées en téléchargement sur GitLab. Si au moins l'une d'elle possède un numéro de version supérieur au vôtre, alors votre cirrus peut (et devrait) être mis à jour.

## Préparer le passage d'une version 1.5.2 (ou inférieure) à une version 1.6.0 (ou supérieure) ##

La version 1.6.0 de cirrus corrige certaines coquilles et décommissionne des fonctionnalités jugées peu pertinentes. Ainsi, pour passer d'une version 1.5.2 (ou inférieure) à une version 1.6.0 (ou supérieure) : 

1. Renommez le dossier "datas" en "data".
2. Renommez le dossier "data/recyle" en "data/recycle".
3. (optionnel) Supprimez le dossier "data/logs" et son contenu.

## Mettre à jour cirrus ##

1. Décompressez l'archive obtenue.
2. Supprimez le contenu de votre installation cirrus actuelle, **à l'exception du dossier "datas" (versions inférieures ou égale à 1.5.2) ou "data" (depuis la version 1.6.0)** : ce dernier contient toutes vos données (fichiers, utilisateurs, invitations, etc.).
3. Copiez-collez les nouveaux fichiers et dossiers de l'archive à l'emplacement de votre installation précédente (au besoin, reportez à la page "[Installer cirrus](./installer-cirrus#doc)").
4. Supprimez l'historique / les données concernant cirrus de votre navigateur web pour garantir la mise à jour côté client (pour information, le service worker a été décommissionné en version 1.9.2 au profit de la mise en cache client traditionnelle).