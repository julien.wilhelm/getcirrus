---
title: 6. Gérer la corbeille
category: "1. Utiliser cirrus"
layout: default
---

La corbeille est un espace intermédiaire destiné aux fichiers et dossiers supprimés depuis le dossier personnel, dernier arrêt avant suppression définitive. Dans cirrus, la corbeille dispose de fonctionnalités limitées. Par exemple, il n'est pas possible d'y créer des dossiers ou d'y téléverser des fichiers. Il n'est pas non plus possible de restaurer des éléments depuis cet emplacement.

Pour autant, la corbeille reste utile puisqu'elle permet :

* De consulter le contenu supprimé.
* De télécharger des éléments supprimés par mégarde.
* De procéder à la suppression définitive desdits éléments. 

Sur cette page :

* [Alterner entre dossier personnel et corbeille](#alterner-entre-dossier-personnel-et-corbeille)
* [Supprimer définitivement un élément](#supprimer-définitivement-un-élément)
* [Vider la corbeille](#vider-la-corbeille)

## Alterner entre dossier personnel et corbeille ##

<span class="article__roles">Rôle requis : propriétaire</span>

* **Vers corbeille** : depuis le dossier personnel, cliquez sur le bouton <svg viewBox="0 0 24 24"><title>Afficher la corbeille</title><path d="M9 19c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm4 0c0 .552-.448 1-1 1s-1-.448-1-1v-10c0-.552.448-1 1-1s1 .448 1 1v10zm5-17v2h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.315c0 .901.73 2 1.631 2h5.712zm-3 4v16h-14v-16h-2v18h18v-18h-2z"></path></svg> situé dans le coin supérieur de l’application pour basculer dans le dossier corbeille.
* **Vers dossier personnel** : depuis le dossier corbeille, cliquez sur le bouton <svg viewBox="0 0 24 24"><title>Afficher les données</title><path d="M12 5c3.453 0 5.891 2.797 5.567 6.78 1.745-.046 4.433.751 4.433 3.72 0 1.93-1.57 3.5-3.5 3.5h-13c-1.93 0-3.5-1.57-3.5-3.5 0-2.797 2.479-3.833 4.433-3.72-.167-4.218 2.208-6.78 5.567-6.78zm0-2c-4.006 0-7.267 3.141-7.479 7.092-2.57.463-4.521 2.706-4.521 5.408 0 3.037 2.463 5.5 5.5 5.5h13c3.037 0 5.5-2.463 5.5-5.5 0-2.702-1.951-4.945-4.521-5.408-.212-3.951-3.473-7.092-7.479-7.092z"></path></svg> situé dans le coin supérieur de l’application pour basculer dans le dossier personnel.

## Supprimer définitivement un élément ##

<span class="article__roles">Rôle requis : propriétaire</span>

**Attention :** toute suppression depuis la corbeille vaut pour suppression définitive.  
Pour supprimer un élément, reportez-vous à la page "[Supprimer un élément](./supprimer-un-element#doc)". 

## Vider la corbeille ##

<span class="article__roles">Rôle requis : propriétaire</span>

Pour vider la corbeille de cirrus, utilisez le bouton <svg viewBox="0 0 24 24"><title>Vider la corbeille (maintenir pour enclencher)</title><path d="M18.5 15c-2.486 0-4.5 2.015-4.5 4.5s2.014 4.5 4.5 4.5c2.484 0 4.5-2.015 4.5-4.5s-2.016-4.5-4.5-4.5zm-.469 6.484l-1.688-1.637.695-.697.992.94 2.115-2.169.697.696-2.811 2.867zm-2.031-12.484v4.501c-.748.313-1.424.765-2 1.319v-5.82c0-.552.447-1 1-1s1 .448 1 1zm-4 0v10c0 .552-.447 1-1 1s-1-.448-1-1v-10c0-.552.447-1 1-1s1 .448 1 1zm1.82 15h-11.82v-18h2v16h8.502c.312.749.765 1.424 1.318 2zm-6.82-16c.553 0 1 .448 1 1v10c0 .552-.447 1-1 1s-1-.448-1-1v-10c0-.552.447-1 1-1zm14-4h-20v-2h5.711c.9 0 1.631-1.099 1.631-2h5.316c0 .901.73 2 1.631 2h5.711v2zm-1 2v7.182c-.482-.115-.983-.182-1.5-.182l-.5.025v-7.025h2z"></path></svg> situé dans le coin supérieur de cirrus. La procédure d'engagement et d'accomplissement de l'action est identique à celle décrite en page "[Supprimer un élément](./supprimer-un-element#doc)" (seul le déclencheur est différent).