---
title: 2. Accéder ou partager un fichier, un dossier avec l'URL
category: "2. Partager cirrus"
layout: default
---

<span class="article__roles">Rôle requis : aucun</span>
<span class="article__version">Version requise : 1.8.0 ou supérieure</span>

Depuis la version 1.8, cirrus permet d'**accéder directement à un dossier ou à un fichier en fonction de l'URL de la barre d'adresse**. Cette fonctionnalité est activée par défaut, apporte de nombreux bénéfices, mais appelle aussi à **prendre des précautions complémentaires pour garantir la sécurité de vos données**.

Sur cette page :

* [Principe et bénéfices](#principe-et-bénéfices)
* [Comprendre la composition de l'URL](#comprendre-la-composition-de-lurl)
* [Considérations importantes relatives à la confidentialité des données](#considérations-importantes-relatives-à-la-confidentialité-des-données)
* [Cas pratiques](#cas-pratiques)
* [Désactiver l'accès par l'URL](#désactiver-laccès-par-lurl)

**Cette fonctionnalité introduit un changement de paradigme important en matière de gestion d'accès au contenu. Il est dans votre intérêt de lire cette page avec attention.**

## Principe et bénéfices ##

Avec cirrus, chaque fois que...

* Vous entrez dans un nouveau dossier.
* Vous ouvrez un fichier en prévisualisation.
* Vous fermez un fichier en prévisualisation.

...L'URL dans la barre d'adresse de votre navigateur web est actualisée. 

Ceci permet : 

* À n'importe quel utilisateur de mettre en favori ou de partager une URL permettant d'accéder directement à une ressource sur cirrus.
* À votre navigateur web de retracer l'historique de votre parcours, ce qui peut vous être utile par la suite.
* De pouvoir recharger la page sans être reconduit à la racine du dossier personnel.

Au cours de votre navigation, cirrus va systématiquement :

* Vérifier que vous êtes connecté d'une façon ou d'une autre (soit [avec un compte qui vous est propre](./comprendre-les-roles#doc), soit en tant que [visiteur](./mettre-en-place-un-lien-access-visiteurs#doc)), pour vous rediriger vers la page d'authentification dans le cas contraire.
* S'assurer que vous ne tentez pas d'accéder à un [dossier protégé](./creer-proteger-supprimer-un-dossier#protéger-un-dossier-et-son-contenu) qui vous est interdit.

Ainsi, si vous entrez dans la barre d'adresse une URL obtenue plus tôt :

1. Il vous est d'abord demandé de vous authentifier si cela n'est pas déjà fait.
2. Puis, une fois authentifié :  
    * Si vous êtes en droit d'accéder au dossier, cirrus vous y conduit. Si un fichier est précisé dans l'URL, cirrus procède ensuite à son ouverture (prévisualisation ou téléchargement selon le type de fichier). 
    * Si vous n'êtes pas autorisé à accéder au dossier, cirrus vous redirige à la racine du dossier personnel (d'où l'intérêt pour le propriétaire de bien [structurer ses dossiers pour garantir la confidentialité des données](./structurer-vos-dossiers-pour-garantir-la-confidentialite-des-donnees#doc)). Dans ce cas de figure, si un fichier avait été spécifié, il n'est pas ouvert.

Notes :

* Si le dossier demandé n'existe pas, cirrus vous redirige à la racine du dossier personnel.
* Si le fichier demandé n'existe pas, celui-ci n'est pas ouvert (logique). 

## Comprendre la composition de l'URL ##

Observons quelques exemples pour démystifier la composition de l'URL dans cirrus. Ces URL sont automatiquement générées par l'application : vous n'avez pas à vous préoccuper outre mesure de leur construction. **Mais il est bon de les comprendre pour savoir ce qu'elles sont susceptibles de provoquer**.

Dans les exemples suivants, cirrus est installé sur un sous-domaine de "votre-domaine.fr".   
L'accès principal à cirrus se fait à l'adresse "https://cirrus.votre-domaine.fr".

* Exemple n° 1 : "https://cirrus.votre-domaine.fr/**?context=content&dir=hello**"
    * `context=content` : indique que vous êtes dans le dossier personnel.
    * `dir=hello` : indique que vous êtes dans le dossier "hello" (du dossier personnel, donc)
* Exemple n° 2 : "https://cirrus.votre-domaine.fr/**?context=content&dir=1%2F2&file=image.jpg**"
    * `context=content` : indique que vous êtes dans le dossier personnel.
    * `dir=1%2F2` : indique que vous êtes dans le dossier "2", lui-même enfant du dossier "1".
    * `file=image.jpg` : indique que le fichier "image.jpg" est ouvert en prévisualisation.
* Exemple n° 3 : "https://cirrus.votre-domaine.fr/**?context=recycle**"
    * `context=recycle` : indique que vous êtes dans la corbeille.
    * L'absence du paramètre `dir` indique par ailleurs que vous êtes à la racine de la corbeille.

En résumé : 

* `context` (obligatoire) : détermine si l'utilisateur se trouve dans le dossier personnel ou dans la corbeille.
* `dir` (optionnel) : détermine dans quel dossier l'utilisateur se trouve. Si absent, l'utilisateur est à la racine.
* `file` (optionnel) : détermine quel fichier est ouvert. Si absent, aucun fichier n'est ouvert.

### URL et lien d'accès visiteur

Lorsque le [lien d'accès visiteurs](./mettre-en-place-un-lien-access-visiteurs#doc) est activé, un quatrième paramètre apparait dans l'URL : 

* `auth` (obligatoire) : numéro d'autorisation associé au lien d'accès visiteurs.

La présence de ce paramètre identifie tout utilisateur non authentifié en tant que visiteur. Aussi, pour partager une ressource dont l'accès a été restreint, pensez à supprimer la partie `&auth=` et tout ce qui se trouve derrière pour exiger la connexion avec un compte utilisateur.

Par exemple :

* Si vous partagez le dossier protégé "DossierProtege" avec l'URL "https://cirrus.votre-domaine.fr/?context=content&dir=dossierProtege**&auth=116b952b4f1d6a4a6a0f8f2578a30c67a550a149e5ad558639cb97099b7f4694316aea211ea5c22cc6a283294acf64573e3218f097bf941f7444684cd7aeb37d**", cirrus considérera que l'utilisateur doit être connecté en tant que visiteurs et l'accès au dossier ne sera pas possible. 
* Pour partager ce dossier dont l'accès est restreint, la bonne URL sera "https://cirrus.votre-domaine.fr/?context=content&dir=dossierProtege". 



## Considérations importantes relatives à la confidentialité des données ##

Historiquement, quand un utilisateur n'était pas autorisé à accéder à un dossier (et, par extension, à son contenu), ce dernier n'était tout simplement pas listé dans cirrus, et il était impossible d'y accéder par quelque moyen que ce soit.

L'accès direct par l'URL change un peu la donne. 

**Bien qu'un dossier protégé le soit toujours autant, ses enfants éventuels ne le sont jamais par défaut. Avec l'URL, un utilisateur peut donc désormais tout à fait y accéder à moins de définir le contraire**.

Imaginons, la structure suivante :

* Dossier "A" (protégé).
* Dossier "A/1" (non protégé).
* Dossier "A/2" (protégé).

Dans ce cas de figure :

* L'accès au dossier "A" et à son enfant "2" ne peut se faire par l'URL à moins d'être le propriétaire du cirrus ou de figurer dans la liste blanche (compte obligatoire).
* L'accès au dossier enfant "1" est possible depuis l'URL pour les utilisateurs authentifiés (avec compte, ou non) alors que son parent, "Dossier A", est protégé !

**La contre-mesure est simple : pensez à restreindre l'accès à tout enfant de dossiers protégés !**

## Cas pratiques ##

L'accès direct par l'URL est une évolution majeure de cirrus. Voici comment répondre à deux cas d'usages courants :

* Transmettre un lien vers une ressource publique à un utilisateur qui n'a pas de compte cirrus.
    1. Activez le [lien d'accès visiteurs](./mettre-en-place-un-lien-access-visiteurs#doc).
    2. Naviguez jusqu'au dossier à partager (y entrer). S'il s'agit d'un fichier, ouvrez-le.
    3. Copiez-collez l'URL de la barre d'adresse.
    4. Partagez l'URL avec la personne concernée. 
* Transmettre un lien vers une ressource protégée à un utilisateur.
    1. [Invitez](./gerer-les-utilisateurs#inviter-un-utilisateur) la personne concernée à se créer un compte sur votre cirrus si elle n'en possède pas déjà un.
    2. [Autorisez-la](./creer-proteger-supprimer-un-dossier#protéger-un-dossier-et-son-contenu) à accéder au dossier concerné.
    3. Naviguez jusqu'au dossier à partager (y entrer). S'il s'agit d'un fichier, ouvrez-le.
    4. Copiez-collez l'URL de la barre d'adresse. Si le lien d'accès visiteur est actif, veillez à supprimer la partie `&auth=` et tout ce qui se trouve derrière pour exiger la connexion avec un compte utilisateur.
    5. Partagez l'URL avec la personne concernée. 

## Désactiver l'accès par l'URL ##

À l'heure actuelle, il n'est pas possible de désactiver l'accès par l'URL. Si toutefois vous ne souhaitez pas bénéficier de cette fonctionnalité, vous pouvez vous rabattre sur la [version 1.7.1 de cirrus](https://gitlab.com/julien.wilhelm/cirrus/-/tags/v1.7.1). 