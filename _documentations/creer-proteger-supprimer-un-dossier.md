---
title: 3. Créer, protéger, supprimer un dossier
category: "1. Utiliser cirrus"
layout: default
---

Sur cette page :

* [Créer un nouveau dossier](#créer-un-nouveau-dossier)
* [Protéger un dossier et son contenu](#protéger-un-dossier-et-son-contenu)
* [Supprimer un dossier](#supprimer-un-dossier)

## Créer un nouveau dossier ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

1. Cliquez sur le bouton <svg viewBox="3 3 18 18"><title>Créer un dossier</title><path d="M12.75 16H11.25V13.75H9V12.25H11.25V10H12.75V12.25H15V13.75H12.75V16Z"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M19.3636 6.63636C20.2674 6.63636 21 7.36899 21 8.27273V18.0909C21 18.9946 20.2674 19.7273 19.3636 19.7273H4.63636C3.73262 19.7273 3 18.9946 3 18.0909V6.63636C3 5.73262 3.73262 5 4.63636 5H9.54545C10.4618 5 11.0451 5.38901 11.631 6.12815C11.6472 6.14858 11.6866 6.19938 11.7303 6.25567C11.792 6.3352 11.8624 6.42596 11.8875 6.45703C11.9129 6.48848 11.9342 6.51449 11.9519 6.53598C12.0239 6.6235 12.0343 6.63619 12.0015 6.63636H19.3636ZM19.3636 18.0909V8.27273L11.9956 8.27272C11.408 8.26957 11.0253 7.99447 10.6139 7.48444C10.5766 7.43825 10.4861 7.32155 10.4203 7.23679C10.3859 7.19243 10.3582 7.15674 10.3486 7.14458C10.0421 6.75792 9.85984 6.63636 9.54545 6.63636H4.63636V18.0909H19.3636Z"></path></svg> situé dans le coin supérieur de cirrus.
2. Renseignez le nom du dossier à créer dans l'invité de saisie qui apparait à l'écran. 
3. Validez l'action avec le bouton `OK` ou la touche `Entrée`.
    * Les caractères spéciaux non supportés seront automatiquement remplacés par des tirets.
    * Si cirrus contient déjà un dossier du même nom que celui saisi à cet emplacement, le nouveau sera renommé pour empêcher toute perte inattendue de contenu. 

## Protéger un dossier et son contenu ## 

<span class="article__roles">Rôle requis : propriétaire ou éditeur (depuis la version 1.8)</span>

**Par défaut, tout utilisateur ayant accès à un cirrus peut accéder au contenu de ce dernier.**

* Les utilisateurs disposant un compte de type [visualiseur](./comprendre-les-roles#visualiseur) ou connectés au moyen d'un [lien d'accès visiteurs](./mettre-en-place-un-lien-access-public#doc) peuvent afficher et télécharger des dossiers et leur contenu.
* Les utilisateurs disposant un compte de type [éditeur](./comprendre-les-roles#éditeur) peuvent afficher, télécharger, renommer, déplacer et mettre à la corbeille des dossiers et leur contenu.

Pour restreindre l'accès à un dossier (et à son contenu), le propriétaire d'un cirrus et les éditeurs peuvent définir des règles d'accès plus strictes. En cliquant sur l'option <svg viewBox="3 3 18 18"><title>Définir les règles d'accès au dossier</title><path d="m21 19.5h-18v-15h5.25c1.2713 1.4565 1.7783 2.25 3 2.25h9.75zm-13.438-13.5h-3.0622v12h15v-9.75h-8.25c-1.7542 0-2.6528-1.041-3.6878-2.25zm7.4378 10.5h-6v-3.75h0.75v-0.75c0-1.242 1.008-2.25 2.25-2.25s2.25 1.008 2.25 2.25v0.75h0.75zm-3.75-4.5v0.75h1.5v-0.75c0-0.414-0.336-0.75-0.75-0.75s-0.75 0.336-0.75 0.75z" stroke-width=".75"/></svg> associée à chaque sous-dossier, le panneau de définition des règles d'accès spécifiques à l'élément sélectionné s'ouvre.

Deux options principales sont disponibles :

* **Dossier public** : toute personne ayant accès à ce cirrus (via un lien d'accès visiteurs, un compte visualiseur, éditeur ou propriétaire) peut accéder au dossier et à son contenu avec les droits qui sont les siens.
* **Dossier privé** : par défaut, seul le compte propriétaire peut accéder au dossier et à son contenu. Certains visualiseurs et éditeurs peuvent toutefois y êtes autorisés, à condition d'inscrire leur nom d'utilisateur dans la liste ci-dessous. Attention : si vous n'être pas le propriétaire de ce cirrus, pensez à inscrire votre nom d'utilisateur pour pouvoir continuer à accéder à ce dossier !

La première option permet à tous les utilisateurs de cette installation cirrus d'accéder au dossier et à son contenu (comportement par défaut).

La seconde interdit par défaut l'accès au dossier et à son contenu à tous les utilisateurs, à l'exception du propriétaire, qui devient par la même occasion le seul à le voir. 

Avec cette dernière option, il est possible d'autoriser certains utilisateurs à accéder au dossier et à son contenu (liste blanche). Pour cela, indiquez dans le champ prévu à cet effet le nom des utilisateurs cirrus concernés, à raison d'un nom d'utilisateur par ligne. Ceux-là doivent bien sûr disposer d'un compte de type visualiseur ou éditeur sur ce cirrus pour être ajoutés (leurs droits restent inchangés).

Enfin, pensez à valider avec le bouton <svg viewBox="0 0 24 24"><title>Enregistrer les changements</title><path d="M9 21.035l-9-8.638 2.791-2.87 6.156 5.874 12.21-12.436 2.843 2.817z"></path></svg> pour que les changements prennent effet.

## Supprimer un dossier ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

Pour supprimer un dossier de cirrus, reportez-vous à la page "[Supprimer un élément](./supprimer-un-element#doc)".