---
title: Technologies
category: "4. À propos"
layout: default
---

Envie d'en savoir plus sur les différentes technologies utiles au projet cirrus ?  
Voici de quoi satisfaire votre curiosité.

Sur cette page :

* [Technologies de l'application cirrus](#technologies-de-lapplication-cirrus)
* [Technologies de la documentation](#technologies-de-lapplication-cirrus)

## Technologies de l'application cirrus ##

Cirrus est **une application web**.  
Il repose sur :

* [HTML](https://developer.mozilla.org/fr/docs/Web/HTML),
* [CSS](https://developer.mozilla.org/fr/docs/Web/CSS) (via le préprocesseur [SASS](https://sass-lang.com/)),
* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) (avec [NodeJs](https://nodejs.org/fr/) et [Gulp](https://gulpjs.com/), pour l'automatisation de la concaténation et de la minification des scripts),
* [JSON](https://www.json.org/json-fr.html), 
* De simples fichiers binaires, sans extension,
* [PHP](https://www.php.net/manual/fr/intro-whatis.php) version 7.4 minimum,
* [Apache2](https://httpd.apache.org/docs/2.4/fr/),
* [Git](https://git-scm.com/) pour la gestion de versions, avec [GitLab](https://about.gitlab.com/) comme hébergeur de dépôt distant.

## Technologies de la documentation ##

[getcirrus.awebsome.fr](https://getcirrus.awebsome.fr) est **un site web statique**.  
Il repose sur :

* Le générateur de sites statiques [Jekyll](https://jekyllrb.com/) (Ruby),
* Du HTML (principalement généré depuis du [Markdown](https://daringfireball.net/projects/markdown/) et du [Liquid](https://shopify.github.io/liquid/)),
* CSS (via le préprocesseur SASS).