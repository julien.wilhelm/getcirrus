---
title: Structurer vos dossiers pour garantir la confidentialité des données
category: "3. Tutoriels"
layout: default
---

<span class="article__roles">Rôle requis : propriétaire</span>

Avec cirrus, **tout individu disposant d'un droit d'entrée peut par défaut consulter le contenu disponible**. Lorsqu'un cirrus expose uniquement des données publiques, ce comportement n'est pas un problème ; dans le cas contraire, certaines précautions doivent être prises pour garantir la confidentialité des informations mises en ligne.

Ce tutoriel décrit une approche en ce sens afin de partir du bon pied.

**Lecture préalable requise** : "[Protéger un dossier et son contenu](./creer-proteger-supprimer-un-dossier.html#protéger-un-dossier-et-son-contenu)".

## De l'art de bien structurer ses répertoires

S'il est tentant (et possible) de placer du contenu public dans le dossier parent, **l'approche par sous-dossiers permet de créer une distinction claire et sécurisante entre contenu public et privé**.

En premier lieu, créez trois dossiers dans le dossier parent de cirrus (intitulés donnés à titre indicatif) :

* **Un dossier "public"** destiné au contenu qui devrait rester accessible à tout utilisateur du cirrus. Les dossiers étant publics par défaut, celui-ci ne requiert pas de configuration supplémentaire.
* **Un dossier "privé"** destiné au contenu auquel seul le propriétaire du cirrus devrait avoir accès. Ce dossier doit être passé en "Dossier privé".
* **Un dossier "partagé"** destiné au contenu à partager avec des utilisateurs spécifiques et disposant obligatoirement d'un compte visualiseur ou éditeur. Ce dossier doit également être passé en "Dossier privé". 

À ce stade, seul le contenu du dossier "public" est ouvert à d'autres qu'au propriétaire du cirrus.

Pour le dossier "privé", seul le propriétaire peut y avoir accès : c'est ni plus ni moins ce que l'on attend de lui.

La gestion du dossier "partagé" demande plus d'attention. Prenons un cas de figure en guise d'exemple. Imaginez travailler avec un clientA et un clientB. Pour des raisons évidentes de confidentialité, clientA ne devrait pouvoir avoir accès aux éléments de votre collaboration avec clientB, et vice-versa.

Voici comment procéder pour travailler sereinement :

1. Créez un sous-dossier privé "clientA" dans "partagé" avec autorisation d'accès pour clientA.
2. Créez un sous-dossier privé "clientB" dans "partagé"  avec autorisation d'accès pour clientB.
3. Autorisez enfin clientA et clientB à accéder au dossier "partagé".

Ainsi, seuls clientA et clientB peuvent accéder au dossier "partagé", et l'un comme l'autre ne peut accéder qu'aux données qui le concernent. 

**Important : tous les utilisateurs de type éditeur peuvent ajouter du contenu sur cirrus ; il est dans l'intérêt du propriétaire d'indiquer quel est l'endroit le plus approprié pour le faire (le cas échéant) afin de garantir la confidentialité des données.**