---
title: Licence
category: "4. À propos"
layout: default
---

Créé par Julien Wilhelm ([Awebsome](https://awebsome.fr/)), cirrus, le Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces, est, depuis la version 1.5.x, **un logiciel libre placé sous licence GPL 3.0**. Vous pouvez [consulter cette licence sur GitLab](https://gitlab.com/julien.wilhelm/cirrus/blob/main/licence.txt) (en anglais).