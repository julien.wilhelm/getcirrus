---
title: 5. Supprimer un élément
category: "1. Utiliser cirrus"
layout: default
---

Cirrus propose **une gestion novatrice des actions dangereuses**.  
Cette page en détaille le fonctionnement.

Sur cette page :

* [Supprimer un élément](#supprimer-un-élément)
* [Méthode alternative](#méthode-alternative-environnements-de-bureau-seuls)

## Supprimer un élément ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

1. Cliquez sur le bouton <svg viewBox="-3 -3 30 30"><title>Supprimer (maintenir pour enclencher)</title><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"></path></svg> associé à l'élément à supprimer ET **maintenez la pression pour amorcer l'action**. Le logo cirrus apparaît au centre de l'écran : l'application évalue désormais le temps écoulé entre le début (l'appui) et la fin (le relâchement) de votre clic.
2. Deux scénarios sont alors possibles :
    1. **Durée de pression inférieure à 600 millisecondes (avant apparition du texte)** : durant ce laps de temps, la commande de suppression n'est pas armée, vous pouvez relâcher le clic sans risque pour abandonner l'action.
    2. **Durée de pression égale ou supérieure à 600 millisecondes (après apparition du texte)** : à ce stade, la commande de suppression est armée, sans être exécutée. 
        * Pour confirmer, relâchez la pression sur le bouton à l'origine de l'action. 
        * Pour annuler, déplacez le curseur à un tout autre endroit avant d'y relâcher la pression.

La vidéo ci-dessous illustre la mise en oeuvre de cette fonctionnalité. 

<video controls preload="none">
    <source src="/assets/videos/supprimer-un-element.mp4"><source>
</video>

On y tout d'abord un premier dossier qui n'est jamais supprimé malgré plusieurs tentatives, l'action étant relâchée ailleurs ou trop tôt. Puis la suppression du second dossier, validée par un clic relâché sur l'option de suppression initiale après au moins 600 millisecondes.

## Méthode alternative (environnements de bureau seuls) ##

<span class="article__roles">Rôle requis : propriétaire ou éditeur</span>

1. Utilisez la navigation au clavier pour vous déplacer dans l'interface utilisateur de cirrus.
2. Pressez la touche `Entrée` lorsque le bouton <svg viewBox="-3 -3 30 30"><title>Supprimer (maintenir pour enclencher)</title><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"></path></svg> associé au dossier à supprimer obtient le focus.
3. Validez l'action avec le bouton `OK` ou la touche `Entrée`.