---
title: "cirrus : Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces"
layout: default
---

Un cirrus est un type de nuage ayant hérité son nom latin de la forme qu'il rappelle, celle d'une boucle de cheveux. Au-delà de l'aspect poétique, c.i.r.r.u.s. est l'acronyme idéal du **Cloud Intègre, Responsable et Résilient pour Utilisateurs Sagaces** porté par [Awebsome](https://awebsome.fr). 

<img src="/assets/images/cirrus-logo.svg" style="width:360px; margin: 0 auto">

Que ce soit pour...

* Disposer d'une solution de **Cloud** en adéquation avec vos convictions personnelles.
* Maîtriser pleinement l'utilisation des données que vous mettez en ligne (**Intègre**).
* Réduire la consommation en données et en énergie de votre patrimoine numérique (**Responsable**).
* Assurer votre autonomie au moyen d'outils conçus sans dépendances (**Résilient**).
* Revendiquer le fait que vous êtes de ces **Utilisateurs Sagaces** qui attendent mieux du Web.

...cirrus vous permet de disposer d'**un espace de stockage sûr et performant pour vos données**.

<a class="cta" href="/documentations/installer-cirrus#doc">Installer cirrus</a>

Envie d'en apprendre plus sur le projet ? La suite de cette page initialement rédigée par Julien Wilhelm en 2021 y est consacrée : 

1. [Turbulences dans le cloud](#turbulences-dans-le-cloud)  
2. [Un cloud écoresponsable ?](#un-cloud-écoresponsable-)  
3. [Un cloud  éthique et performant !](#un-cloud-éthique-et-performant-)  
4. [Mais à qui s'adresse cirrus ?](#mais-à-qui-sadresse-cirrus-)  

## Turbulences dans le cloud

L'essor des outils numériques et la généralisation de la surveillance en ligne soulèvent **des interrogations légitimes** au sein de la communauté des utilisateurs du Web.

* **Qui** d'autre que nous accède aux informations privées que nous mettons en ligne ? 
* **Comment** surveille-t-on nos faits et gestes au quotidien sur Internet ? 
* **Pourquoi** ?

Comme tout individu soucieux de **protéger, par principe, ses données personnelles**, j'ai moi-même tourné le dos aux GAFAM et je me suis enquis d'alternatives logicielles plus respectueuses de ma vie privée (et de celles de mes clients, au passage). Et d'en arriver à la conclusion suivante : en matière de cloud loyal, [NextCloud](https://nextcloud.com/), fork du logiciel libre [OwnCloud](https://owncloud.com/), domine un marché d'une diversité navrante.

### Le problème avec NextCloud

NextCloud est **un ambitieux projet open source**, de ceux-là qui tirent joyeusement  la langue à l'adage "Si c'est gratuit, c'est vous le produit". En tant que promoteur de la sobriété numérique, je ne peux cependant pas me résigner à l'utiliser.

Deux raisons à cela : 

* Trop de fonctionnalités sont proposées par défaut pour un usage modeste. 
* Sa conception le rend à la fois énergivore côté serveur et côté client. 

J'ai essayé NextCloud avec insistance (OwnCloud aussi, d'ailleurs). Quand les installations fastidieuses ont laissé place aux lenteurs et autres instabilités à l'utilisation,  j'ai préféré retrousser mes manches et réfléchir à **la création d'une alternative aux alternatives**. Un peu comme je l'ai fait avec [WordsMatter](https://gitlab.com/julien.wilhelm/WordsMatter), le module de gestion de blog 100% autonome, libre et gratuit estampillé Awebsome.

## Un cloud écoresponsable ?

Soyons clairs : quelle qu'elle soit, **l'utilisation d'un cloud reste polluante** puisqu'il s'agit de répliquer, dans des équipements étrangers aux siens, des données qu'il faudra transmettre et recevoir grâce à Internet (spoiler : il n'y a aucune magie là-dedans, ces données consomment de l'énergie pour voyager tout en contribuant au déclin des infrastructures impliquées).

**Un cloud peut toutefois se révéler plus utile que destructeur dès lors que son usage est raisonné**. Nous sommes en effet nombreux à y trouver des intérêts justifiés, le plus souvent dans un cadre professionnel. En ce qui me concerne, son usage est strictement réservé aux collaborations [Awebsome](https://awebsome.fr). Pour permettre à mes clients la mise à disposition des médias essentiels à la création de leur site web écoresponsable et de mon côté leur donner la possibilité d'observer la progression de mon travail, un **espace de partage temporaire** se prête tout à fait à l'exercice.

Et depuis début 2021, c'est avec cirrus que cela se passe.

## Un cloud éthique et performant !

### La simplicité comme moteur de résilience

Plutôt que de parler de cloud écoresponsable, parlons de **cloud responsable** !

Cirrus tire toute sa force dans la **simplicité logicielle** qui caractérise les développements web Awebsome. Conçu dans une logique de sobriété numérique, **son poids ridicule d'environ 150Ko par instance**, contre plusieurs centaines de méga-octets pour un NextCloud, en dit déjà long, même s'il faut reconnaître que les possibilités des deux outils restent inégales.

<figure>
	<a href="/assets/images/cirrus-accueil-1366.png">
		<picture>
			<source media="(min-width:1440px)" srcset="/assets/images/cirrus-accueil-768.png">
			<source media="(min-width:1120px)" srcset="/assets/images/cirrus-accueil-480.png">
			<source media="(min-width:960px)" srcset="/assets/images/cirrus-accueil-320.png">
			<source media="(min-width:800px)" srcset="/assets/images/cirrus-accueil-700.png">
			<source media="(min-width:580px)" srcset="/assets/images/cirrus-accueil-480.png">
			<source media="(min-width:420px)" srcset="/assets/images/cirrus-accueil-320.png">
			<img src="/assets/images/cirrus-accueil-264.png" alt="Capture d'écran cirrus." loading="lazy">
		</picture>
	</a>
	<figcaption>Écran de connexion cirrus : une étape nécessaire pour protéger l'accès à ses données personnelles.</figcaption>
</figure>

Comme toujours dans mon travail, l'application fait cavalier seul côté dépendances, une course à l'efficience ouvrant la voie à **une simplification radicale des processus de déploiement et de maintenance**. Vous ne renoncerez jamais à cirrus faute de place. Mieux : si vous savez faire un copier-coller, vous saurez l'installer en quelques secondes.

Tout ce dont vous avez besoin, c'est d'un serveur web prenant en charge PHP 7+ et d'un navigateur, de préférence de confiance, tel que [Firefox](https://www.mozilla.org/fr/firefox/new/). Vous n'aurez pas non plus besoin d'une quelconque base de données : cirrus repose sur **un système de fichiers plats** (flat-files).

### Ethic by design

La transparence est un critère important lorsque l'on parle d'intégrité. Les affirmations sont vérifiables au-delà des belles paroles, il suffit d'aller vérifier par soi-même si les promesses sont tenues. Impossible donc pour cirrus, qui entend **être un modèle éthique et un outil de résilience**, de faire l'impasse sur l'aspect **open source**.

Sur Internet, le respect de la vie privée des utilisateurs passe par **la mise en œuvre de modèles économiques ne reposant pas sur la collecte de données personnelles** (et surtout pas à leur insu). Un logiciel ne fait jamais rien que l'on ne lui ait dit de faire ; aussi, rendre un service numérique intègre n'est pas bien compliqué en soi. 

<figure>
	<a href="/assets/images/cirrus-conso-1366.png">
		<picture>
			<source media="(min-width:1440px)" srcset="/assets/images/cirrus-conso-768.png">
			<source media="(min-width:1120px)" srcset="/assets/images/cirrus-conso-480.png">
			<source media="(min-width:960px)" srcset="/assets/images/cirrus-conso-320.png">
			<source media="(min-width:800px)" srcset="/assets/images/cirrus-conso-700.png">
			<source media="(min-width:580px)" srcset="/assets/images/cirrus-conso-480.png">
			<source media="(min-width:420px)" srcset="/assets/images/cirrus-conso-320.png">
			<img src="/assets/images/cirrus-conso-264.png" alt="Capture d'écran cirrus." loading="lazy">
		</picture>
	</a>
	<figcaption>Économe en requêtes serveur, cirrus ne fait pas de zèle avec les données personnelles de ses utilisateurs.</figcaption>
</figure>

Il est de notoriété publique que *faire de l'argent* sur la base d'un projet open source est compliqué. Précisément parce que le code source est ouvert et qu'il est difficile de déterminer qui fait quoi avec. Mais renoncer à toute forme de rétribution n'est pas une fatalité. De la même manière que l'on peut attendre des concepteurs de produits que certains agissent avec exemplarité, l'on peut compter sur l'honnêteté d'une manne d'utilisateurs pour honorer leurs _obligations_. C'est le pari qu'a fait l'équipe de [Kirby](https://getkirby.com/) en livrant le code source de son CMS et en conditionnant son exploitation à l'**acquisition d'une licence**. Un pari bâti sur **une relation de confiance**, dont je me suis d'abord inspiré... avant de placer cirrus dans le domaine **du logiciel libre et gratuit** ;) !

### Quid de l'expérience utilisateur ?

S'agissant en somme d'un explorateur de fichiers et de dossiers connecté, cirrus permet d'effectuer tout ce que l'on est en droit d'attendre de lui : **mise en ligne**, **téléchargement**, **déplacement**, **copie**, **renommage**, **suppression**... Bien sûr, il était trop tentant de vouloir dépoussiérer le genre pour s'y refuser. Le **thème sombre alternatif**, le **100% responsive** et la possibilité d'installer cirrus en tant que **Progressive Web Application (PWA)** sur son téléphone, c'est du déjà-vu ; il fallait trouver mieux, sans sombrer dans l'ébriété numérique. 

Dans cirrus, il est demandé de maintenir un temps donné la pression sur le bouton correspondant d'un même élément pour armer la commande de suppression, puis de relâcher au même endroit pour valider, ou ailleurs pour désamorcer l'action. De quoi bousculer agréablement nos habitudes en matière d'expérience utilisateur (UX). 

<figure>
	<a href="/assets/images/cirrus-suppression-1366.png">
		<picture>
			<source media="(min-width:1440px)" srcset="/assets/images/cirrus-suppression-768.png">
			<source media="(min-width:1120px)" srcset="/assets/images/cirrus-suppression-480.png">
			<source media="(min-width:960px)" srcset="/assets/images/cirrus-suppression-320.png">
			<source media="(min-width:800px)" srcset="/assets/images/cirrus-suppression-700.png">
			<source media="(min-width:580px)" srcset="/assets/images/cirrus-suppression-480.png">
			<source media="(min-width:420px)" srcset="/assets/images/cirrus-suppression-320.png">
			<img src="/assets/images/cirrus-suppression-264.png" alt="Capture d'écran cirrus." loading="lazy">
		</picture>
	</a>
	<figcaption>La suppression d'éléments selon cirrus. Exit les pop-up de confirmation liés aux actions dangereuses.</figcaption>
</figure>

L'on voit sur cette même capture que l'**accessibilité visuelle** a été prise en compte avec des repères signalant les éléments concernés par une action ou ayant le focus. Dans un registre commun, cirrus est **entièrement navigable au clavier**.

Au-delà de l'exploration, certains formats de fichiers peuvent même être ouverts directement depuis l'interface.

<figure>
	<a href="/assets/images/cirrus-previsualisation-pdf-1366.png">
		<picture>
			<source media="(min-width:1440px)" srcset="/assets/images/cirrus-previsualisation-pdf-768.png">
			<source media="(min-width:1120px)" srcset="/assets/images/cirrus-previsualisation-pdf-480.png">
			<source media="(min-width:960px)" srcset="/assets/images/cirrus-previsualisation-pdf-320.png">
			<source media="(min-width:800px)" srcset="/assets/images/cirrus-previsualisation-pdf-700.png">
			<source media="(min-width:580px)" srcset="/assets/images/cirrus-previsualisation-pdf-480.png">
			<source media="(min-width:420px)" srcset="/assets/images/cirrus-previsualisation-pdf-320.png">
			<img src="/assets/images/cirrus-previsualisation-pdf-264.png" alt="Capture d'écran cirrus." loading="lazy">
		</picture>
	</a>
	<figcaption>Un PDF sur les retours clients Awebsome, ici en prévisualisation dans cirrus.</figcaption>
</figure>

De nombreux formats sont d'ores et déjà supportés de façon native par nos navigateurs : **les plus utiles seront intégrés à cirrus**. Quant aux autres... Eh bien, il faudra repasser ! Le traitement de documents de type office est impossible sans dépendances massives ; il s'agit une voie que cirrus n'empruntera pas.

### L'heure des comptes (ou presque)

Je ne pouvais terminer ce tour d'horizon sans évoquer la **gestion des droits d'accès**.

Si, par défaut, une instance de cirrus n'est ouverte qu'à son propriétaire, plusieurs options sont bien sûr au programme pour étendre son accès à d'autres. Il est ainsi très simple de **gérer les membres d'une même instance cirrus** depuis le panneau d'administration. 

Voici pour les options disponibles  :

* Inviter de nouveaux **visualiseurs** à s'inscrire (droits de lecture sur les fichiers et les dossiers).
* Inviter de nouveaux **éditeurs** à s'inscrire (droits de lecture et d'écriture sur les fichiers et les dossiers).
* Créer ou désactiver **un lien d'accès public** à l'instance (droit de lecture, aucune inscription requise).

<figure>
	<a href="/assets/images/cirrus-admin-1366.png">
		<picture>
			<source media="(min-width:1440px)" srcset="/assets/images/cirrus-admin-768.png">
			<source media="(min-width:1120px)" srcset="/assets/images/cirrus-admin-480.png">
			<source media="(min-width:960px)" srcset="/assets/images/cirrus-admin-320.png">
			<source media="(min-width:800px)" srcset="/assets/images/cirrus-admin-700.png">
			<source media="(min-width:580px)" srcset="/assets/images/cirrus-admin-480.png">
			<source media="(min-width:420px)" srcset="/assets/images/cirrus-admin-320.png">
			<img src="/assets/images/cirrus-admin-264.png" alt="Capture d'écran cirrus." loading="lazy">
		</picture>
	</a>
	<figcaption>Le panneau d'administration de cirrus. Peu de fonctionnalités, mais des fonctionnalités utiles.</figcaption>
</figure>

Vous restez dans tous les cas maître de votre cirrus et vous vous réservez le droit de **révoquer un membre** ou d'**alterner le rôle** qui est le sien. Et si vous souhaitez contrôler avec plus de granularité qui accède à quoi, **cirrus permet de verrouiller un dossier et de choisir qui peut y avoir accès**.

## Mais à qui s'adresse cirrus ?

De par son faible gabarit et ses intentions (trop ?) louables, **cirrus réjouira les classes utilisatrices misant sur la simplicité, l'efficience et le contrôle de leurs outils numériques**. 

Récapitulatif de ses points forts : 

* Il est **ultra léger** et **très performant**.
* C'est **un outil fiable** auquel l'on peut faire confiance.
* L'utiliser, plutôt qu'un autre, permet de **limiter les conséquences environnementales de son activité en ligne** (hors effet rebond, bien sûr).

Cependant, veuillez garder à l'esprit que cirrus :

* N'est pas destiné à un usage collaboratif en temps réel. Et il ne le sera jamais.
* Demeure avant tout un logiciel destiné à **promouvoir la sobriété numérique**, aux fonctionnalités essentielles.

cirrus est aujourd'hui adopté par des particuliers, de petites entreprises et même par certaines collectivités. 

<a class="cta" href="/documentations/installer-cirrus#doc">Installer cirrus</a>